class Board
  attr_reader :grid

  # DISPLAY_HASH = {
  #   s: " ",
  #   x: "x",
  #   nil => " "
  # }
  #
  # SHIP_HASH = {
  #   aircraft_carrier: 5x1,
  #   battleship: 4x1,
  #   submarine: 3x1,
  #   destroyer: 2x1,
  #   patrol_boat: 1x1,
  # }

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    grid.flatten.select { |el| el == :s }.length
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    grid[x][y] = val
  end

  def empty?(pos = nil)
    if pos
      x, y = pos
      grid[x][y].nil?
    else
      count == 0
    end
  end

  def full?
    occupied = grid.flatten.select { |el| el unless nil }
    occupied.length == grid.flatten.length
  end

  def place_random_ship
    raise "Board is full" if full?
    pos = random_pos
    until empty?(pos)
      pos = random_pos
    end
    self[pos] = :s
  end

  def random_pos
    [rand(grid.length), rand(grid.length)]
  end

  def won?
    !grid.flatten.any? { |el| el == :s }
  end
end

# if __FILE__ == $PROGRAM_NAME
#   a = Board.new
#   a[3, 4]
# end
