class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Make a move. [x, y]"
    gets.chomp
  end
end
